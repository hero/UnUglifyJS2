#! /usr/bin/env node
// -*- js -*-

"use strict";

// workaround for tty output truncation upon process.exit()
[process.stdout, process.stderr].forEach(function (stream) {
    if (stream._handle && stream._handle.setBlocking)
        stream._handle.setBlocking(true);
});

var fs = require("fs");
var info = require("../package.json");
var path = require("path");
var program = require("commander");
var UglifyJS = require("../lib/all_in_one");

var skip_keys = ["cname", "enclosed", "parent_scope", "scope", "thedef", "uses_eval", "uses_with"];

var defaultCompressOption = {
    "booleans": true,
    "cascade": true,
    "collapse_vars": true,
    "comparisons": true,
    "conditionals": true,
    "dead_code": true,
    "drop_console": false,
    "drop_debugger": true,
    "evaluate": true,
    "expression": false,
    "hoist_funs": true,
    "hoist_vars": false,
    "ie8": false,
    "if_return": true,
    "inline": true,
    "join_vars": true,
    "keep_fargs": true,
    "keep_fnames": false,
    "keep_infinity": false,
    "loops": true,
    "negate_iife": true,
    "passes": 1,
    "properties": true,
    "pure_funcs": null,
    "reduce_vars": true,
    "sequences": true,
    "side_effects": true,
    "switches": true,
    "top_retain": null,
    "toplevel": false,
    "typeofs": true,
    "unsafe": false,
    "unsafe_comps": false,
    "unsafe_Func": false,
    "unsafe_math": false,
    "unsafe_proto": false,
    "unsafe_regexp": false,
    "unused": true,
    "warnings": false
};

var options = {
    compress: true,
    mangle: true,
    output: {
        ast: false,//打印抽象语法树，而不打印代码
        code: true
    }
};


run();

function convert_ast(fn) {
    return UglifyJS.AST_Node.from_mozilla_ast(Object.keys(files).reduce(fn, null));
}

function run() {
    UglifyJS.AST_Node.warn_function = function (msg) {
        print_error("WARN: " + msg);
    };

    var codeSource = fs.readFileSync("/Users/virjar/git/UglifyJS2/test/test2.js", "utf8");

    //program.output = "ast";//测试代码，打印抽象语法树的标记

    var result = UglifyJS.minifyString(codeSource, options);

    if (result.error) {
        var ex = result.error;
        if (ex.name == "SyntaxError") {
            print_error("Parse error at " + ex.filename + ":" + ex.line + "," + ex.col);
            var col = ex.col;
            var lines = files[ex.filename].split(/\r?\n/);
            var line = lines[ex.line - 1];
            if (!line && !col) {
                line = lines[ex.line - 2];
                col = line.length;
            }
            if (line) {
                var limit = 70;
                if (col > limit) {
                    line = line.slice(col - limit);
                    col = limit;
                }
                print_error(line.slice(0, 80));
                print_error(line.slice(0, col).replace(/\S/g, " ") + "^");
            }
        }
        if (ex.defs) {
            print_error("Supported options:");
            print_error(format_object(ex.defs));
        }
        fatal(ex);
    } else if (program.output == "ast") {
        print(JSON.stringify(result.ast, function (key, value) {
            if (skip_key(key)) return;
            if (value instanceof UglifyJS.AST_Token) return;
            if (value instanceof UglifyJS.Dictionary) return;
            if (value instanceof UglifyJS.AST_Node) {
                var result = {
                    _class: "AST_" + value.TYPE
                };
                value.CTOR.PROPS.forEach(function (prop) {
                    result[prop] = value[prop];
                });
                return result;
            }
            return value;
        }, 2));
    } else if (program.output == "spidermonkey") {
        print(JSON.stringify(UglifyJS.minify(result.code, {
            compress: false,
            mangle: false,
            output: {
                ast: true,
                code: false
            }
        }).ast.to_mozilla_ast(), null, 2));
    } else if (program.output) {
        fs.writeFileSync(program.output, result.code);
        if (result.map) {
            fs.writeFileSync(program.output + ".map", result.map);
        }
    } else {
        print(result.code);
    }
    if (program.nameCache) {
        fs.writeFileSync(program.nameCache, JSON.stringify(options.nameCache));
    }
    if (result.timings) for (var phase in result.timings) {
        print_error("- " + phase + ": " + result.timings[phase].toFixed(3) + "s");
    }
}

function fatal(message) {
    if (message instanceof Error) message = message.stack.replace(/^\S*?Error:/, "ERROR:")
    print_error(message);
    process.exit(1);
}

// A file glob function that only supports "*" and "?" wildcards in the basename.
// Example: "foo/bar/*baz??.*.js"
// Argument `glob` may be a string or an array of strings.
// Returns an array of strings. Garbage in, garbage out.
function simple_glob(glob) {
    if (Array.isArray(glob)) {
        return [].concat.apply([], glob.map(simple_glob));
    }
    if (glob.match(/\*|\?/)) {
        var dir = path.dirname(glob);
        try {
            var entries = fs.readdirSync(dir);
        } catch (ex) {
        }
        if (entries) {
            var pattern = "^" + path.basename(glob)
                    .replace(/[.+^$[\]\\(){}]/g, "\\$&")
                    .replace(/\*/g, "[^/\\\\]*")
                    .replace(/\?/g, "[^/\\\\]") + "$";
            var mod = process.platform === "win32" ? "i" : "";
            var rx = new RegExp(pattern, mod);
            var results = entries.filter(function (name) {
                return rx.test(name);
            }).map(function (name) {
                return path.join(dir, name);
            });
            if (results.length) return results;
        }
    }
    return [glob];
}

function read_file(path, default_value) {
    try {
        return fs.readFileSync(path, "utf8");
    } catch (ex) {
        if (ex.code == "ENOENT" && default_value != null) return default_value;
        fatal(ex);
    }
}

function parse_js(flag) {
    return function (value, options) {
        options = options || {};
        try {
            UglifyJS.minify(value, {
                parse: {
                    expression: true
                },
                compress: false,
                mangle: false,
                output: {
                    ast: true,
                    code: false
                }
            }).ast.walk(new UglifyJS.TreeWalker(function (node) {
                if (node instanceof UglifyJS.AST_Assign) {
                    var name = node.left.print_to_string();
                    var value = node.right;
                    if (flag) {
                        options[name] = value;
                    } else if (value instanceof UglifyJS.AST_Array) {
                        options[name] = value.elements.map(to_string);
                    } else {
                        options[name] = to_string(value);
                    }
                    return true;
                }
                if (node instanceof UglifyJS.AST_Symbol || node instanceof UglifyJS.AST_PropAccess) {
                    var name = node.print_to_string();
                    options[name] = true;
                    return true;
                }
                if (!(node instanceof UglifyJS.AST_Sequence)) throw node;

                function to_string(value) {
                    return value instanceof UglifyJS.AST_Constant ? value.getValue() : value.print_to_string({
                        quote_keys: true
                    });
                }
            }));
        } catch (ex) {
            if (flag) {
                fatal("Error parsing arguments for '" + flag + "': " + value);
            } else {
                options[value] = null;
            }
        }
        return options;
    }
}

function parse_source_map() {
    var parse = parse_js();
    return function (value, options) {
        var hasContent = options && "content" in options;
        var settings = parse(value, options);
        if (!hasContent && settings.content && settings.content != "inline") {
            print_error("INFO: Using input source map: " + settings.content);
            settings.content = read_file(settings.content, settings.content);
        }
        return settings;
    }
}

function skip_key(key) {
    return skip_keys.indexOf(key) >= 0;
}

function format_object(obj) {
    var lines = [];
    var padding = "";
    Object.keys(obj).map(function (name) {
        if (padding.length < name.length) padding = Array(name.length + 1).join(" ");
        return [name, JSON.stringify(obj[name])];
    }).forEach(function (tokens) {
        lines.push("  " + tokens[0] + padding.slice(tokens[0].length - 2) + tokens[1]);
    });
    return lines.join("\n");
}

function print_error(msg) {
    process.stderr.write(msg);
    process.stderr.write("\n");
}

function print(txt) {
    process.stdout.write(txt);
    process.stdout.write("\n");
}

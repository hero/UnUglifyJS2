/**
 * Created by virjar on 17/4/9.
 */

function showKeyboard() {
    $("#keyboard_mask").css({
        display: "block",
        height: $(window).height()
    });
    $("#keyboard_val").css("width", $("#keyboard_content").width() - 8);
    var e = ($(window).width() - $("#keyboard_content").width()) / 2
    var n = ($(window).height() - $("#keyboard_content").height()) / 2;
    $("#keyboard_content").css({
        left: e,
        top: n
    });
    $("#keyboard_content").css("display", "block");
}
